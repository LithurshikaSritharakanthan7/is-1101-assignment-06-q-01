#include <stdio.h>

int patt(int i);
int row(int j);
int NOR=1;


int patt(int i)
{

    if(i>0)
    {
        row(NOR);
        printf("\n");
        NOR++;
        patt(i-1);

    }
    return 0;
}


int row(int j)
{
    if(j>0)
    {
        printf("%d",j);
        row(j-1);
    }
    return 0;
}

int main()
{
    int num;
    printf("Enter the no. of rows needed to obtain the pattern: ");
    scanf("%d",&num);
    patt(num);
    return 0;
}
